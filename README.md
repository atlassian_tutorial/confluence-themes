# Tutorial: 

You can find the full instructions for this tutorial here: [Writing a Confluence Theme][1].

## Running locally

To run this app locally, make sure that you have the [Atlassian Plugin SDK][2] installed, and then run:

    atlas-mvn confluence:run
    
 [1]: https://developer.atlassian.com/display/CONFDEV/Writing+a+Confluence+Theme
 [2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project